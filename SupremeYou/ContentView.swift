//
//  ContentView.swift
//  SupremeYou
//
//  Created by Pedro Emmanuel Ayala Rodriguez on 19/11/19.
//  Copyright © 2019 Pedro Emmanuel Ayala Rodriguez. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
